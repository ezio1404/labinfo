<?php

function dbconn(){
    try{
        return new PDO("mysql:hostname=localhost;dbname=labinfo_db","root","");
    }catch(PDOExecption $e){
        echo $e->getMessage();
    } 
}//end dbconn()

function destroy(){
    return null;
}//end of destroy()

function addTest($data){
    $ok;
    $db=dbconn();
    $sql="INSERT INTO tbl_test(test_name,test_price) values(?,?)";
    $stmt=$db->prepare($sql);
    $ok=$stmt->execute($data);
    $db=destroy();
    return $ok;
}// end of addTest

function getAllTest(){
    $dbconn=dbconn();
    $sql="SELECT * FROM tbl_test";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute();
    $rows=$stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn=destroy();
    return $rows;
}// end of getAllTest

function getTest($data){
    $dbconn=dbconn();
    $sql="SELECT * FROM tbl_test WHERE test_id=?";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute($data);
    $rows=$stmt->fetch(PDO::FETCH_ASSOC);
    $dbconn=destroy();
    return $rows;
}// end of getTest

function updateTest($data){
    $ok;
    $db=dbconn();
    $sql="UPDATE tbl_test SET test_name=?,test_price=? WHERE test_id=?";
    $stmt=$db->prepare($sql);
    $ok=$stmt->execute($data);
    $db=destroy(); 
    return $ok;
}// end of updateTest

function deleteTest($data){
    $ok;
    $db=dbconn();
    $sql="DELETE FROM tbl_test WHERE test_id=?";
    $stmt=$db->prepare($sql);
    $ok=$stmt->execute($data);				
    return $ok;
}// end of deleteTest

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function addDoctor($data){
    $ok;
    $db=dbconn();
    $sql="INSERT INTO tbl_doctor(doctor_image,doctor_fname,doctor_lname,doctor_qualification,doctor_classification,doctor_email,doctor_phone) values(?,?,?,?,?,?,?)";
    $stmt=$db->prepare($sql);
    $ok=$stmt->execute($data);
    $db=destroy();
    return $ok;
}// end of addDoctor


function updateDoctor($data){
    $ok;
    $db=dbconn();
    $sql="UPDATE tbl_doctor SET doctor_image=?,doctor_fname=? ,doctor_lname=? ,doctor_qualification=? ,doctor_classification=? ,doctor_email=?,doctor_phone=? WHERE doctor_id=?";
    $stmt=$db->prepare($sql);
    $ok=$stmt->execute($data);
    $db=destroy();
    return $ok;
}// end of updateDoctor


function getAllDoctor(){
    $dbconn=dbconn();
    $sql="SELECT * FROM tbl_doctor";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute();
    $rows=$stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn=destroy();
    return $rows;
}// end of getAllDoctor

function getDoctor($data){
    $dbconn=dbconn();
    $sql="SELECT * FROM tbl_doctor WHERE doctor_id=?";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute($data);
    $rows=$stmt->fetch(PDO::FETCH_ASSOC);
    $dbconn=destroy();
    return $rows;
}// end of getDoctor

function deleteDoctor($data){
    $ok;
    $db=dbconn();
    $sql="DELETE FROM tbl_doctor WHERE doctor_id=?";
    $stmt=$db->prepare($sql);
    $ok=$stmt->execute($data);				
    return $ok;
}// end of deleteDoctor

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function addPatient($data){
    $ok;
    $db=dbconn();
    $sql="INSERT INTO tbl_patient(patient_fname,patient_lname,patient_age,patient_sex,patient_addr,patient_email,patient_phone) values(?,?,?,?,?,?,?)";
    $stmt=$db->prepare($sql);
    $ok=$stmt->execute($data);
    $db=destroy();
    return $ok;
}// end of addPatient

function getLatestPatient(){
    $dbconn=dbconn();
    $sql="SELECT MAX(patient_id) as patient_id FROM tbl_patient ";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute($data);
    $rows=$stmt->fetch(PDO::FETCH_ASSOC);
    $dbconn=destroy();
    return $rows;
}// end of getLatestPatient

function addDoctorPatient($data){
    $ok;
    $db=dbconn();
    $sql="INSERT INTO tbl_doctorpatient(doctor_id,patient_id) values(?,?)";
    $stmt=$db->prepare($sql);
    $ok=$stmt->execute($data);
    $db=destroy();
    return $ok;
}// end of addDoctorPatient

function addTestOfPatient($data){
    $ok;
    $db=dbconn();
    $sql="INSERT INTO tbl_testofpatient(test_id,patient_id) values(?,?)";
    $stmt=$db->prepare($sql);
    $ok=$stmt->execute($data);
    $db=destroy();
    return $ok;
}// end of addTestOfPatient

function getAllPatient(){
    $dbconn=dbconn();
    $sql="SELECT * FROM tbl_patient";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute();
    $rows=$stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn=destroy();
    return $rows;
}// end of getAllPatient

function deletePatient($data){
    $ok;
    $db=dbconn();
    $sql="DELETE FROM tbl_patient WHERE patient_id=?";
    $stmt=$db->prepare($sql);
    $ok=$stmt->execute($data);				
    return $ok;
}// end of deletePatient

function getPatient($data){
    $dbconn=dbconn();
    $sql="SELECT * FROM tbl_patient WHERE patient_id=?";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute($data);
    $rows=$stmt->fetch(PDO::FETCH_ASSOC);
    $dbconn=destroy();
    return $rows;
}// end of getPatient



function getAllTestOfPatient($data){
    $dbconn=dbconn();
    $sql="SELECT * FROM tbl_patient p,tbl_testofpatient top,tbl_test t WHERE p.patient_id = top.patient_id AND t.test_id =top.test_id AND p.patient_id = ?";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute($data);
    $rows=$stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn=destroy();
    return $rows;
}// end of getAllTestOfPatient


function getTestOfPatient($data){ 
    $dbconn=dbconn();
    $sql="SELECT * FROM tbl_test t,tbl_testofpatient top,tbl_patient p WHERE p.patient_id = top.patient_id AND t.test_id = top.test_id  AND t.test_id = ? AND  p.patient_id = ?";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute($data);
    $rows=$stmt->fetch(PDO::FETCH_ASSOC);
    $dbconn=destroy();
    return $rows;
}// end of getTestOfPatient

function deleteTestOfPatient($data){
    $ok;
    $db=dbconn();
    $sql="DELETE FROM tbl_testofpatient WHERE top_id=?";
    $stmt=$db->prepare($sql);
    $ok=$stmt->execute($data);				
    return $ok;
}// end of deleteTestOfPatient

function updateTestOfPatient($data){
    $ok;
    $db=dbconn();
    $sql="UPDATE tbl_testofpatient top SET top.test_value=? WHERE top.top_id=?";
    $stmt=$db->prepare($sql);
    $ok=$stmt->execute($data);
    $db=destroy(); 
    return $ok;
}// end of updateTestOfPatient


function getAllDoctorPatient($data){
    $dbconn=dbconn();
    $sql="SELECT * FROM tbl_patient p,tbl_doctor d, tbl_doctorpatient dp WHERE d.doctor_id = dp.doctor_id AND p.patient_id= dp.patient_id AND d.doctor_id = ?";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute($data);
    $rows=$stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn=destroy();
    return $rows;
}// end of getAllDoctorPatient