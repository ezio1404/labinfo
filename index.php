<?php
session_start();
if($_SESSION){
    header("Location:dashboard.php?message=");
}
?>
<!doctype html>
<html lang="en">


<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Medicare +</title>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.png" />
    <link rel="stylesheet" href="plugin/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="plugin/bootstrap/css/bootstrap-theme.css">
    <link rel="stylesheet" href="fonts/poppins/poppins.css">
    <link rel="stylesheet" href="plugin/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="plugin/jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" href="plugin/process-bar/tox-progress.css">
    <link rel="stylesheet" href="plugin/owl-carouse/owl.carousel.min.css">
    <link rel="stylesheet" href="plugin/owl-carouse/owl.theme.default.min.css">
    <link rel="stylesheet" href="plugin/animsition/css/animate.css">
    <link rel="stylesheet" href="plugin/jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" href="plugin/mediaelement/mediaelementplayer.css">
    <link rel="stylesheet" href="plugin/datetimepicker/bootstrap-datepicker3.css">
    <link rel="stylesheet" href="plugin/datetimepicker/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="plugin/lightgallery/lightgallery.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>

    <!--load page-->
    <div class="load-page">
        <div class="sk-wave">
            <div class="sk-rect sk-rect1"></div>
            <div class="sk-rect sk-rect2"></div>
            <div class="sk-rect sk-rect3"></div>
            <div class="sk-rect sk-rect4"></div>
            <div class="sk-rect sk-rect5"></div>
        </div>
    </div>

    <!-- Mobile nav -->
    <nav class="visible-sm visible-xs mobile-menu-container mobile-nav">
        <div class="menu-mobile-nav navbar-toggle">
            <span class="icon-bar"><i class="fa fa-bars" aria-hidden="true"></i></span>
        </div>
        <div id="cssmenu" class="animated">
            <div class="uni-icons-close"><i class="fa fa-times" aria-hidden="true"></i></div>
            <ul class="nav navbar-nav animated">
                <li class="active"><a href="index.php">Home</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="contact.php">Contact</a></li>
                <li><a href="gallery.php">Gallery</a></li>
                <li><a href="departments.php">Departments</a></li>
                <li><a href="doctors.php">Doctors</a></li>

                <li><a href="login.php?message=">Login</a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
    </nav>
    <!-- End mobile menu -->

    <div class="uni-home-1">
        <div id="wrapper-container" class="site-wrapper-container">
            <header>
                <div class="uni-medicare-header sticky-menu">
                    <div class="container">
                        <div class="uni-medicare-header-main">
                            <div class="row">
                                <div class="col-md-2">
                                    <!--LOGO-->
                                    <div class="wrapper-logo">
                                        <a class="logo-default" href="#"><img src="images/logo.png" alt=""
                                                class="img-responsive"></a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <!--MENU TEXT-->
                                    <div class="uni-main-menu">
                                        <nav class="main-navigation uni-menu-text">
                                            <div class="cssmenu">
                                                <ul>
                                                    <li class="active"><a href="index.php">Home</a></li>
                                                    <li><a href="about.php">About</a></li>
                                                    <li><a href="contact.php">Contact</a></li>
                                                    <li><a href="gallery.php">Gallery</a></li>
                                                    <li><a href="departments.php">Departments</a></li>
                                                    <li><a href="doctors.php">Doctors</a></li>
                                                    <li><a href="login.php?message=">Login</a></li>
                                                </ul>
                                            </div>
                                        </nav>
                                    </div>


                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </header>

            <div id="main-content" class="site-main-content">
                <section class="site-content-area">

                    <!--BANNER-->
                    <div class="uni-banner">
                        <div class="uni-owl-one-item owl-carousel owl-theme">
                            <div class="item">
                                <div class="uni-banner-img uni-background-5"></div>
                                <div class="content animated" data-animation="flipInX" data-delay="0.9s">
                                    <div class="container">
                                        <div class="caption">
                                            <h1>Let us protect your health</h1>
                                            <p>
                                                Amazing Things Are Happening Here.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="uni-banner-img uni-background-6"></div>
                                <div class="content animated" data-animation="flipInX" data-delay="0.9s">
                                    <div class="container">
                                        <div class="caption">
                                            <h1>Let us protect your health</h1>
                                            <p>
                                                Building a Healthy Community One Individual at a Time.
                                                <br>
                                                Bringing the future of healthcare.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="uni-banner-img uni-background-7"></div>
                                <div class="content animated" data-animation="flipInX" data-delay="0.9s">
                                    <div class="container">
                                        <div class="caption">
                                            <h1>Let us protect your health</h1>
                                            <p>
                                                Compassionate Care, Advanced Medicine, Close to Home.
                                                <br>
                                                Committed to Caring Since 1966.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--OPENING HOURS AND BOOK APPOINTMENT-->
                    <div class="uni-home-opening-book">
                        <div class="container">
                            <div class="uni-home-opening-book-content">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="uni-services-opinging-hours">
                                            <div class="uni-services-opinging-hours-title">
                                                <div class="icon">
                                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                </div>
                                                <h4>opening hours</h4>
                                            </div>
                                            <div class="uni-services-opinging-hours-content">
                                                <table class="table">
                                                    <tr>
                                                        <td>monday</td>
                                                        <td>8:00 - 17:00</td>
                                                    </tr>
                                                    <tr>
                                                        <td>tuesday</td>
                                                        <td>8:00 - 17:00</td>
                                                    </tr>
                                                    <tr>
                                                        <td>wednesday</td>
                                                        <td>8:00 - 17:00</td>
                                                    </tr>
                                                    <tr>
                                                        <td>thursday</td>
                                                        <td>8:00 - 17:00</td>
                                                    </tr>
                                                    <tr>
                                                        <td>friday</td>
                                                        <td>8:00 - 17:00</td>
                                                    </tr>
                                                    <tr>
                                                        <td>sunday</td>
                                                        <td>8:00 - 17:00</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="uni-single-department-appointment-form">

                                            <div class="uni-home-title">
                                                <h3>Book appoitment</h3>
                                                <div class="uni-underline"></div>
                                            </div>

                                            <form action="#">
                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <div class="input-group form-group">
                                                            <span class="input-group-addon"><i class="fa fa-user"
                                                                    aria-hidden="true"></i></span>
                                                            <input type="text" class="form-control" name="name" value=""
                                                                placeholder="your name">
                                                        </div>
                                                        <div class="input-group form-group">
                                                            <span class="input-group-addon"><i class="fa fa-phone"
                                                                    aria-hidden="true"></i></span>
                                                            <input type="tel" class="form-control" name="phone" value=""
                                                                placeholder="phone number">
                                                        </div>
                                                        <div class="input-group form-group">
                                                            <span class="input-group-addon"><i class="fa fa-envelope"
                                                                    aria-hidden="true"></i></span>
                                                            <input type="email" class="form-control" name="email"
                                                                value="" placeholder="email">
                                                        </div>
                                                        <div class="input-group form-group">
                                                            <div class="input-group date date-check-in"
                                                                data-date="12-02-2017" data-date-format="mm-dd-yyyy">
                                                                <span class="input-group-addon"><i
                                                                        class="fa fa-calendar"
                                                                        aria-hidden="true"></i></span>
                                                                <input name="date1" class="form-control" type="text"
                                                                    value="12-02-2017">
                                                                <span class="input-group-addon btn"><i
                                                                        class="fa fa-calendar" id="ti-calendar1"
                                                                        aria-hidden="true"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-7">
                                                        <div class="input-group form-group">
                                                            <textarea id="message" style="resize: none;" name="phone"
                                                                class="form-control" placeholder="note"></textarea>
                                                        </div>
                                                        <button class="vk-btn vk-btn-send">send</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!--DEPARTMENT-->
                    <div class="uni-hơm-1-department">
                        <div class="container">
                            <div class="uni-home-title">
                                <h3>Department</h3>
                                <div class="uni-underline"></div>
                            </div>
                            <div class="uni-shortcode-icon-box-1">
                                <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <div class="uni-shortcode-icon-box-1-default">
                                            <div class="item-icons">
                                                <img src="images/icons_box/icon_1/icon-5.png" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="item-caption">
                                                <h4>cardiology</h4>
                                                <p>study and treatment of disorders of the heart and the blood vessels.
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-6">
                                        <div class="uni-shortcode-icon-box-1-default">
                                            <div class="item-icons">
                                                <img src="images/icons_box/icon_1/icon-4.png" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="item-caption">
                                                <h4>Neurology</h4>
                                                <p> treat and manage disorders that affect the central nervous system
                                                    and the peripheral nervous system.</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-6">
                                        <div class="uni-shortcode-icon-box-1-default">
                                            <div class="item-icons">
                                                <img src="images/icons_box/icon_1/icon-3.png" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="item-caption">
                                                <h4>Orthopedics</h4>
                                                <p>the branch of medicine dealing with the correction of deformities of
                                                    bones or muscles.</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-6">
                                        <div class="uni-shortcode-icon-box-1-default">
                                            <div class="item-icons">
                                                <img src="images/icons_box/icon_1/icon-2.png" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="item-caption">
                                                <h4>cancer department</h4>
                                                <p>a disease caused by an uncontrolled division of abnormal cells in a
                                                    part of the body.</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-6">
                                        <div class="uni-shortcode-icon-box-1-default">
                                            <div class="item-icons">
                                                <img src="images/icons_box/icon_1/icon-1.png" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="item-caption">
                                                <h4>Ophthalmology</h4>
                                                <p>the branch of medicine concerned with the study and treatment of
                                                    disorders and diseases of the eye.</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-6">
                                        <div class="uni-shortcode-icon-box-1-default">
                                            <div class="item-icons">
                                                <img src="images/icons_box/icon_1/icon.png" alt=""
                                                    class="img-responsive">
                                            </div>
                                            <div class="item-caption">
                                                <h4>Respiratory</h4>
                                                <p>relating to or affecting respiration or the organs of respiration.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--OUR DOCTOR-->
                    <div class="uni-home-1-our-doctor">
                        <div class="uni-shortcode-team-2 uni-background-2">
                            <div class="container">

                                <div class="uni-home-title">
                                    <h3>Our Doctor</h3>
                                    <div class="uni-underline"></div>
                                </div>

                                <div class="uni-owl-four-item owl-carousel owl-theme">
                                    <div class="item">
                                        <div class="uni-team-default">
                                            <div class="item-img">
                                                <img src="images/doctor/img-1.jpg" alt="" class="img-responsive">
                                            </div>
                                            <div class="item-caption">
                                                <div class="col-md-3 col-sm-3 col-xs-3 uni-clear-padding">
                                                    <div class="item-icons">
                                                        <img src="images/icons_box/icon_4/icon-5.png" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-9 col-sm-9 col-xs-9 uni-clear-padding">
                                                    <div class="item-title">
                                                        <h4>adam jonson</h4>
                                                        <span>Cardiologist</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="uni-team-default">
                                            <div class="item-img">
                                                <img src="images/doctor/img-1.jpg" alt="" class="img-responsive">
                                            </div>
                                            <div class="item-caption">
                                                <div class="col-md-3 col-sm-3 col-xs-3 uni-clear-padding">
                                                    <div class="item-icons">
                                                        <img src="images/icons_box/icon_4/icon-4.png" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-9 col-sm-9 col-xs-9 uni-clear-padding">
                                                    <div class="item-title">
                                                        <h4>Henrik larssom</h4>
                                                        <span>neurologist</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="uni-team-default">
                                            <div class="item-img">
                                                <img src="images/doctor/img-1.jpg" alt="" class="img-responsive">
                                            </div>
                                            <div class="item-caption">
                                                <div class="col-md-3 col-sm-3 col-xs-3 uni-clear-padding">
                                                    <div class="item-icons">
                                                        <img src="images/icons_box/icon_4/icon-3.png" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-9 col-sm-9 col-xs-9 uni-clear-padding">
                                                    <div class="item-title">
                                                        <h4>amanda smith</h4>
                                                        <span>Ophthalmology doctor</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="uni-team-default">
                                            <div class="item-img">
                                                <img src="images/doctor/img-1.jpg" alt="" class="img-responsive">
                                            </div>
                                            <div class="item-caption">
                                                <div class="col-md-3 col-sm-3 col-xs-3 uni-clear-padding">
                                                    <div class="item-icons">
                                                        <img src="images/icons_box/icon_4/icon-2.png" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-9 col-sm-9 col-xs-9 uni-clear-padding">
                                                    <div class="item-title">
                                                        <h4>david martin</h4>
                                                        <span>Cancer doctor</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="uni-team-default">
                                            <div class="item-img">
                                                <img src="images/doctor/img-1.jpg" alt="" class="img-responsive">
                                            </div>
                                            <div class="item-caption">
                                                <div class="col-md-3 col-sm-3 col-xs-3 uni-clear-padding">
                                                    <div class="item-icons">
                                                        <img src="images/icons_box/icon_4/icon-5.png" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-9 col-sm-9 col-xs-9 uni-clear-padding">
                                                    <div class="item-title">
                                                        <h4>adam jonson</h4>
                                                        <span>Cardiologist</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="uni-team-default">
                                            <div class="item-img">
                                                <img src="images/doctor/img-1.jpg" alt="" class="img-responsive">
                                            </div>
                                            <div class="item-caption">
                                                <div class="col-md-3 col-sm-3 col-xs-3 uni-clear-padding">
                                                    <div class="item-icons">
                                                        <img src="images/icons_box/icon_4/icon-4.png" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-9 col-sm-9 col-xs-9 uni-clear-padding">
                                                    <div class="item-title">
                                                        <h4>Henrik larssom</h4>
                                                        <span>neurologist</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!--OUR SERVICES-->
                    <div class="uni-home-our-services">
                        <div class="uni-shortcode-icons-box-5">
                            <div class="container">

                                <div class="uni-home-title">
                                    <h3>Our Services</h3>
                                    <div class="uni-underline"></div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <div class="uni-shortcode-icons-box-5-default">
                                            <div class="item-icons-title">
                                                <div class="col-md-4 uni-clear-padding">
                                                    <div class="item-icons">
                                                        <img src="images/icons_box/icon_4/icon-5.png" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-8 uni-clear-padding">
                                                    <div class="item-title">
                                                        <h4>Corneal transplant surgery</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item-caption">
                                                <p>
                                                    A cornea transplant (keratoplasty) is a surgical procedure to
                                                    replace part of your cornea with corneal tissue from a donor. Your
                                                    cornea is the transparent, dome-shaped surface of your eye that
                                                    accounts for a large part of your eye's focusing power.
                                                </p>
                                                <a href="#" class="readmore">Read more</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <div class="uni-shortcode-icons-box-5-default">
                                            <div class="item-icons-title">
                                                <div class="col-md-4 uni-clear-padding">
                                                    <div class="item-icons">
                                                        <img src="images/icons_box/icon_4/icon-4.png" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-8 uni-clear-padding">
                                                    <div class="item-title">
                                                        <h4>Cardiothoracic Surgery</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item-caption">
                                                <p>
                                                    Cardiothoracic surgery (also known as thoracic surgery) is the field
                                                    of medicine involved in surgical treatment of organs
                                                    inside the thorax (the chest)—generally treatment of conditions of
                                                    the heart (heart disease) and lungs (lung disease). </p>
                                                <a href="#" class="readmore">Read more</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <div class="uni-shortcode-icons-box-5-default">
                                            <div class="item-icons-title">
                                                <div class="col-md-4 uni-clear-padding">
                                                    <div class="item-icons">
                                                        <img src="images/icons_box/icon_4/icon-3.png" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-8 uni-clear-padding">
                                                    <div class="item-title">
                                                        <h4>General health check</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item-caption">
                                                <p>
                                                    Regular health checks can identify any early signs of health issues.
                                                    When you have a health check,
                                                    your doctor will talk with you about your medical history,
                                                    your family's history of disease and your lifestyle, including your
                                                    diet,
                                                    weight, physical activity, alcohol use and whether you smoke.</p>
                                                <a href="#" class="readmore">Read more</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <div class="uni-shortcode-icons-box-5-default">
                                            <div class="item-icons-title">
                                                <div class="col-md-4 uni-clear-padding">
                                                    <div class="item-icons">
                                                        <img src="images/icons_box/icon_4/icon-2.png" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-8 uni-clear-padding">
                                                    <div class="item-title">
                                                        <h4>Diagnosis &amp; <br> treatment of cancer</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item-caption">
                                                <p>
                                                    Common adjuvant therapies include chemotherapy, radiation
                                                    therapy and hormone therapy. Palliative treatment. Palliative
                                                    treatments may help
                                                    relieve side effects of treatment or signs and symptoms caused by
                                                    cancer itself.
                                                    Surgery, radiation, chemotherapy and hormone therapy can all be used
                                                    to relieve signs and symptoms.</p>
                                                <a href="#" class="readmore">Read more</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <div class="uni-shortcode-icons-box-5-default">
                                            <div class="item-icons-title">
                                                <div class="col-md-4 uni-clear-padding">
                                                    <div class="item-icons">
                                                        <img src="images/icons_box/icon_4/icon-1.png" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-8 uni-clear-padding">
                                                    <div class="item-title">
                                                        <h4>treatment of <br> pneumonia</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item-caption">
                                                <p>
                                                    Your doctor will start by asking about your medical history and
                                                    doing a physical exam,
                                                    including listening to your lungs with a
                                                    stethoscope to check for abnormal bubbling or crackling sounds that
                                                    suggest pneumonia.</p>
                                                <a href="#" class="readmore">Read more</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <div class="uni-shortcode-icons-box-5-default">
                                            <div class="item-icons-title">
                                                <div class="col-md-4 uni-clear-padding">
                                                    <div class="item-icons">
                                                        <img src="images/icons_box/icon_4/icon.png" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-8 uni-clear-padding">
                                                    <div class="item-title">
                                                        <h4>Treatment of <br> dermatitis</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item-caption">
                                                <p>
                                                    Mild skin inflammations usually respond to over-the-counter
                                                    hydrocortisone cream. To reduce inflammation and heal the irritation
                                                    of most types of dermatitis, a doctor usually recommends a
                                                    prescription corticosteroid cream and might prescribe an oral
                                                    antihistamine to relieve severe
                                                    itching
                                                </p>
                                                <a href="#" class="readmore">Read more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>







                </section>
            </div>

            <footer class="site-footer footer-default">
                <div class="copyright-area">
                    <div class="container">
                        <div class="copyright-content">
                            <div class="row">
                                <div class="col-sm-6">
                                </div>
                                <div class="col-sm-6">
                                    <ul class="copyright-menu">
                                        <li><a href="#"></a></li>
                                        <li><a href="#"></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <script src="plugin/jquery/jquery-2.0.2.min.js"></script>
    <script src="plugin/jquery-ui/jquery-ui.min.js"></script>
    <script src="plugin/bootstrap/js/bootstrap.js"></script>
    <script src="plugin/process-bar/tox-progress.js"></script>
    <script src="plugin/waypoint/jquery.waypoints.min.js"></script>
    <script src="plugin/counterup/jquery.counterup.min.js"></script>
    <script src="plugin/owl-carouse/owl.carousel.min.js"></script>
    <script src="plugin/jquery-ui/jquery-ui.min.js"></script>
    <script src="plugin/mediaelement/mediaelement-and-player.js"></script>
    <script src="plugin/masonry/masonry.pkgd.min.js"></script>
    <script src="plugin/datetimepicker/moment.min.js"></script>
    <script src="plugin/datetimepicker/bootstrap-datepicker.min.js"></script>
    <script src="plugin/datetimepicker/bootstrap-datepicker.tr.min.js"></script>
    <script src="plugin/datetimepicker/bootstrap-datetimepicker.js"></script>
    <script src="plugin/datetimepicker/bootstrap-datetimepicker.fr.js"></script>

    <script src="plugin/lightgallery/picturefill.min.js"></script>
    <script src="plugin/lightgallery/lightgallery.js"></script>
    <script src="plugin/lightgallery/lg-pager.js"></script>
    <script src="plugin/lightgallery/lg-autoplay.js"></script>
    <script src="plugin/lightgallery/lg-fullscreen.js"></script>
    <script src="plugin/lightgallery/lg-zoom.js"></script>
    <script src="plugin/lightgallery/lg-hash.js"></script>
    <script src="plugin/lightgallery/lg-share.js"></script>
    <script src="plugin/sticky/jquery.sticky.js"></script>

    <script src="js/main.js"></script>
</body>

</html>