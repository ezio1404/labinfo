<?php
include 'dbhelper.php';


if(isset($_POST['btnAddTest']) || isset($_POST['btnUpdateTest']) ){
    $test_name = htmlentities(trim($_POST['test_name']));
    $test_price = htmlentities(trim($_POST['test_price']));

    $data = array($test_name,$test_price);

     if(isset($_POST['btnAddTest'])){
        header("Location:manageTest.php?message=".(addTest($data) == 1?"success_adding":"failed_adding")."&data=".$test_name);
     }if(isset($_POST['btnUpdateTest'])){
        array_push($data,$_GET['id']);
        header("Location:manageTest.php?message=".(updateTest($data) == 1?"success_updating":"failed_updating")."&data=".$test_name);        
     }  
}
if(isset($_POST['btnUpdateTestOfPatient'])){
   $test_value = htmlentities(trim($_POST['test_value']));
   $data= array($test_value,$_GET['topid']);
   header("Location:viewPatient.php?id=".$_GET['patientid']."&message=".(updateTestOfPatient($data) == 1?"success_adding":"failed_adding")."&data=Test value");
}