<?php
include 'dbhelper.php';
if(isset($_POST['btnAddPatient'])){
        $test_id = $_POST['test_id'];
        if(count($test_id)==0){
            header("Location:dashboard.php?message=testisempty");
        }
        $doctor_id= $_POST['referred_doctor'];
        $patient_fname= htmlentities(trim($_POST['patient_fname']));
        $patient_lname= htmlentities(trim($_POST['patient_lname']));
        $patient_age= htmlentities(trim($_POST['patient_age']));
        $patient_sex= htmlentities(trim($_POST['patient_sex']));
        $patient_addr= htmlentities(trim($_POST['patient_addr']));
        $patient_email= htmlentities(trim($_POST['patient_email']));
        $patient_phone= htmlentities(trim($_POST['patient_phone']));

        $data=array($patient_fname,$patient_lname,$patient_age,$patient_sex,$patient_addr,$patient_email,$patient_phone);


     
        addPatient($data);
        $latestPatientID =getLatestPatient();

        addDoctorPatient(array( $doctor_id,$latestPatientID['patient_id']));

        foreach($test_id as $test){
            echo $test;
            addTestOfPatient(array($test,$latestPatientID['patient_id']));
        }


        header("Location:viewPatient.php?id=".$latestPatientID['patient_id']."&message=");

}