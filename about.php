<?php
session_start();
if($_SESSION){
    header("Location:dashboard.php?message=");
}
?>
<!doctype html>
<html lang="en">


<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Medicare +</title>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.png" />
    <link rel="stylesheet" href="plugin/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="plugin/bootstrap/css/bootstrap-theme.css">
    <link rel="stylesheet" href="fonts/poppins/poppins.css">
    <link rel="stylesheet" href="plugin/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="plugin/jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" href="plugin/process-bar/tox-progress.css">
    <link rel="stylesheet" href="plugin/owl-carouse/owl.carousel.min.css">
    <link rel="stylesheet" href="plugin/owl-carouse/owl.theme.default.min.css">
    <link rel="stylesheet" href="plugin/animsition/css/animate.css">
    <link rel="stylesheet" href="plugin/jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" href="plugin/mediaelement/mediaelementplayer.css">
    <link rel="stylesheet" href="plugin/datetimepicker/bootstrap-datepicker3.css">
    <link rel="stylesheet" href="plugin/datetimepicker/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="plugin/lightgallery/lightgallery.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>

    <!--load page-->
    <div class="load-page">
        <div class="sk-wave">
            <div class="sk-rect sk-rect1"></div>
            <div class="sk-rect sk-rect2"></div>
            <div class="sk-rect sk-rect3"></div>
            <div class="sk-rect sk-rect4"></div>
            <div class="sk-rect sk-rect5"></div>
        </div>
    </div>

    <!-- Mobile nav -->
    <nav class="visible-sm visible-xs mobile-menu-container mobile-nav">
        <div class="menu-mobile-nav navbar-toggle">
            <span class="icon-bar"><i class="fa fa-bars" aria-hidden="true"></i></span>
        </div>
        <div id="cssmenu" class="animated">
            <div class="uni-icons-close"><i class="fa fa-times" aria-hidden="true"></i></div>
            <ul class="nav navbar-nav animated">
                <li><a href="index.php">Home</a></li>
                <li class="active"><a href="about.php">About</a></li>
                <li><a href="contact.php">Contact</a></li>
                <li><a href="gallery.php">Gallery</a></li>
                <li><a href="departments.php">Departments</a></li>
                <li><a href="doctors.php">Doctors</a></li>
                <li><a  href="login.php?message=">Login</a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
    </nav>
    <!-- End mobile menu -->

    <div class="uni-about">
        <div id="wrapper-container" class="site-wrapper-container">
            <header>
                <div class="uni-medicare-header sticky-menu">
                    <div class="container">
                        <div class="uni-medicare-header-main">
                            <div class="row">
                                <div class="col-md-2">
                                    <!--LOGO-->
                                    <div class="wrapper-logo">
                                        <a class="logo-default" href="#"><img src="images/logo.png" alt=""
                                                class="img-responsive"></a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <!--MENU TEXT-->
                                    <div class="uni-main-menu">
                                        <nav class="main-navigation uni-menu-text">
                                            <div class="cssmenu">
                                                <ul>
                                                        <li><a href="index.php">Home</a></li>
                                                        <li class="active"><a href="about.php">About</a></li>
                                                        <li><a href="contact.php">Contact</a></li>
                                                        <li><a href="gallery.php">Gallery</a></li>
                                                        <li><a href="departments.php">Departments</a></li>
                                                        <li><a href="doctors.php">Doctors</a></li>
                                                        <li><a  href="login.php?message=">Login</a></li>
                                                </ul>
                                            </div>
                                        </nav>
                                    </div>


                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </header>

            <div id="main-content" class="site-main-content">
                <section class="site-content-area">

                    <div class="uni-banner-default uni-background-1">
                        <div class="container">
                            <!-- Page title -->
                            <div class="page-title">
                                <div class="page-title-inner">
                                    <h1>about</h1>
                                </div>
                            </div>
                            <!-- End page title -->

                            <!-- Breadcrumbs -->
                            <ul class="breadcrumbs">
                                <li><a href="#">home</a></li>
                                <li><a href="#">about</a></li>
                            </ul>
                            <!-- End breadcrumbs -->
                        </div>
                    </div>

                    <div class="uni-about-body">

                        <!--WHO WE ARE-->
                        <div class="uni-about-who-are-you">
                            <div class="container">
                                <div class="uni-services-title">
                                    <h3>who we are</h3>
                                    <div class="uni-underline"></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="uni-about-who-are-you-left">
                                            <img src="images/about/img.jpg" alt="" class="img-responsive">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="uni-about-who-are-you-right">
                                            <p>
                                                Pellentesque habitant morbi trPellentesque habitant morbi tristique
                                                senectus et netus et malesuada fames ac turpis egestas. Vestibulum
                                                tortor
                                                quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu
                                                libero
                                                sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris
                                                placerat
                                                eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra.
                                            </p>
                                            <h4>our missions</h4>
                                            <ul>
                                                <li>Lorem ipsum dolor sit amet, consectetuer</li>
                                                <li>Aliquam tincidunt mauris eu risus</li>
                                                <li>Vestibulum auctor dapibus neque</li>
                                                <li>Morbi in sem quis dui placerat ornare</li>
                                                <li>Donec eu libero sit amet quam egestas semper</li>
                                                <li>Aliquam tincidunt mauris eu risus</li>
                                                <li>Vestibulum auctor dapibus neque</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--Team 2-->
                        <div class="uni-shortcode-team-2 uni-background-2">
                            <div class="container">
                                <div class="uni-services-title">
                                    <h3>Our doctor</h3>
                                    <div class="uni-underline"></div>
                                </div>
                                <div class="uni-owl-four-item owl-carousel owl-theme">
                                    <div class="item">
                                        <div class="uni-team-default">
                                            <div class="item-img">
                                                <img src="images/doctor/img-2.jpg" alt="" class="img-responsive">
                                            </div>
                                            <div class="item-caption">
                                                <div class="col-md-3 col-sm-3 col-xs-3 uni-clear-padding">
                                                    <div class="item-icons">
                                                        <img src="images/icons_box/icon_4/icon-5.png" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-9 col-sm-9 col-xs-9 uni-clear-padding">
                                                    <div class="item-title">
                                                        <h4>adam jonson</h4>
                                                        <span>Cardiologist</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="uni-team-default">
                                            <div class="item-img">
                                                <img src="images/doctor/img-2.jpg" alt="" class="img-responsive">
                                            </div>
                                            <div class="item-caption">
                                                <div class="col-md-3 col-sm-3 col-xs-3 uni-clear-padding">
                                                    <div class="item-icons">
                                                        <img src="images/icons_box/icon_4/icon-4.png" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-9 col-sm-9 col-xs-9 uni-clear-padding">
                                                    <div class="item-title">
                                                        <h4>henrik larsson</h4>
                                                        <span>neurologist</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="uni-team-default">
                                            <div class="item-img">
                                                <img src="images/doctor/img-2.jpg" alt="" class="img-responsive">
                                            </div>
                                            <div class="item-caption">
                                                <div class="col-md-3  col-sm-3 col-xs-3 uni-clear-padding">
                                                    <div class="item-icons">
                                                        <img src="images/icons_box/icon_4/icon-3.png" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-9  col-sm-9 col-xs-9 uni-clear-padding">
                                                    <div class="item-title">
                                                        <h4>amanda smith</h4>
                                                        <span>Ophthalmology doctor</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="uni-team-default">
                                            <div class="item-img">
                                                <img src="images/doctor/img-2.jpg" alt="" class="img-responsive">
                                            </div>
                                            <div class="item-caption">
                                                <div class="col-md-3 col-sm-3 col-xs-3 uni-clear-padding">
                                                    <div class="item-icons">
                                                        <img src="images/icons_box/icon_4/icon-2.png" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-9  col-sm-9 col-xs-9 uni-clear-padding">
                                                    <div class="item-title">
                                                        <h4>david martin</h4>
                                                        <span>Cancer doctor</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="uni-team-default">
                                            <div class="item-img">
                                                <img src="images/doctor/img-2.jpg" alt="" class="img-responsive">
                                            </div>
                                            <div class="item-caption">
                                                <div class="col-md-3 col-sm-3 col-xs-3 uni-clear-padding">
                                                    <div class="item-icons">
                                                        <img src="images/icons_box/icon_4/icon-5.png" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-9 col-sm-9 col-xs-9 uni-clear-padding">
                                                    <div class="item-title">
                                                        <h4>adam jonson</h4>
                                                        <span>Cardiologist</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="uni-team-default">
                                            <div class="item-img">
                                                <img src="images/doctor/img-2.jpg" alt="" class="img-responsive">
                                            </div>
                                            <div class="item-caption">
                                                <div class="col-md-3 col-sm-3 col-xs-3 uni-clear-padding">
                                                    <div class="item-icons">
                                                        <img src="images/icons_box/icon_4/icon-4.png" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-9 col-sm-9 col-xs-9 uni-clear-padding">
                                                    <div class="item-title">
                                                        <h4>henrik larsson</h4>
                                                        <span>neurologist</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--ICONS BOX 3-->
                        <div class="uni-shortcode-icons-box-3">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="uni-shortcode-icons-box-3-default">
                                            <div class="item-icons">
                                                <i class="fa fa-user-md" aria-hidden="true"></i>
                                            </div>
                                            <div class="item-caption">
                                                <h4>Qualified Doctors</h4>
                                                <div class="uni-line"></div>
                                                <p>
                                                    Pellentesque habitant morbi tristique senectus et netus et malesuada
                                                    fame ac turpis egestas. Vestibulum tortor quam.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="uni-shortcode-icons-box-3-default">
                                            <div class="item-icons">
                                                <i class="fa fa-clock-o" aria-hidden="true"></i>
                                            </div>
                                            <div class="item-caption">
                                                <h4>
                                                    24 hours service
                                                </h4>
                                                <div class="uni-line"></div>
                                                <p>
                                                    Pellentesque habitant morbi tristique
                                                    senectus et netus et malesuada fame ac turpis egestas. Vestibulum
                                                    tortor quam.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="uni-shortcode-icons-box-3-default">
                                            <div class="item-icons">
                                                <i class="fa fa-hospital-o" aria-hidden="true"></i>
                                            </div>
                                            <div class="item-caption">
                                                <h4>
                                                    modern equipment
                                                </h4>
                                                <div class="uni-line"></div>
                                                <p>
                                                    Pellentesque habitant morbi tristique
                                                    senectus et netus et malesuada fame ac turpis egestas. Vestibulum
                                                    tortor quam.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="uni-shortcode-icons-box-3-default">
                                            <div class="item-icons">
                                                <i class="fa fa-heart" aria-hidden="true"></i>
                                            </div>
                                            <div class="item-caption">
                                                <h4>
                                                    conscientious Doctors
                                                </h4>
                                                <div class="uni-line"></div>
                                                <p>
                                                    Pellentesque habitant morbi tristique
                                                    senectus et netus et malesuada fame ac turpis egestas. Vestibulum
                                                    tortor quam.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="uni-shortcode-icons-box-3-default">
                                            <div class="item-icons">
                                                <i class="fa fa-ambulance" aria-hidden="true"></i>
                                            </div>
                                            <div class="item-caption">
                                                <h4>
                                                    call in 10 minutes
                                                </h4>
                                                <div class="uni-line"></div>
                                                <p>
                                                    Pellentesque habitant morbi tristique
                                                    senectus et netus et malesuada fame ac turpis egestas. Vestibulum
                                                    tortor quam.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="uni-shortcode-icons-box-3-default">
                                            <div class="item-icons">
                                                <i class="fa fa-commenting" aria-hidden="true"></i>
                                            </div>
                                            <div class="item-caption">
                                                <h4>
                                                    enthusiastic support
                                                </h4>
                                                <div class="uni-line"></div>
                                                <p>
                                                    Pellentesque habitant morbi tristique
                                                    senectus et netus et malesuada fame ac turpis egestas. Vestibulum
                                                    tortor quam.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      
                        <!--OUR SERVICES 2-->
                        <div class="uni-our-services-2 uni-background-4">
                            <div class="container">
                                <div class="uni-services-title">
                                    <h3>Our services</h3>
                                    <div class="uni-underline"></div>
                                </div>

                                <div class="uni-our-service-2-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="tab-nav">
                                                <ul class="nav-pills nav-stacked">
                                                    <li class="active"><a href="#tab_a"
                                                            data-toggle="pill">Cardiothoracic Surgery</a></li>
                                                    <li><a href="#tab_b" data-toggle="pill">Corneal transplant
                                                            surgery</a></li>
                                                    <li><a href="#tab_c" data-toggle="pill">General health check</a>
                                                    </li>
                                                    <li><a href="#tab_d" data-toggle="pill">Diagnosis & treatment
                                                            cancer</a></li>
                                                    <li><a href="#tab_e" data-toggle="pill">Treatment of dermatitis</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="tab_a">
                                                    <div class="uni-our-service-2-content-default">
                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                <div class="item-img">
                                                                    <img src="images/services/img.jpg" alt=""
                                                                        class="img-responsive">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-7">
                                                                <div class="item-caption">
                                                                    <div class="item-caption-title">
                                                                        <h3>Cardiothoracic Surgery</h3>
                                                                        <div class="uni-line"></div>
                                                                    </div>
                                                                    <p>Pellentesque habitant morbi tristique senectus et
                                                                        netus
                                                                        et malesuada fames ac turpis egestas. Vestibulum
                                                                        tortor
                                                                        quam, feugiat vitae, ultricies eget, tempor sit
                                                                        amet,
                                                                        ante. Donec eu libero sit amet quam egestas
                                                                        semper.
                                                                        Aenean ultricies mi vitae est. Mauris placerat
                                                                        eleifend
                                                                        leo.
                                                                    </p>
                                                                    <ul>
                                                                        <li>
                                                                            Lorem ipsum dolor sit amet, consectetuer
                                                                        </li>
                                                                        <li>
                                                                            Aliquam tincidunt mauris eu risus
                                                                        </li>
                                                                        <li>
                                                                            Vestibulum auctor dapibus neque
                                                                        </li>
                                                                        <li>
                                                                            Morbi in sem quis dui placerat ornare
                                                                        </li>
                                                                        <li>
                                                                            Donec eu libero sit amet quam egestas semper
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab_b">
                                                    <div class="uni-our-service-2-content-default">
                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                <div class="item-img">
                                                                    <img src="images/services/img1.jpg" alt=""
                                                                        class="img-responsive">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-7">
                                                                <div class="item-caption">
                                                                    <div class="item-caption-title">
                                                                        <h3>Corneal transplant surgery</h3>
                                                                        <div class="uni-line"></div>
                                                                    </div>
                                                                    <p>Pellentesque habitant morbi tristique senectus et
                                                                        netus
                                                                        et malesuada fames ac turpis egestas. Vestibulum
                                                                        tortor
                                                                        quam, feugiat vitae, ultricies eget, tempor sit
                                                                        amet,
                                                                        ante. Donec eu libero sit amet quam egestas
                                                                        semper.
                                                                        Aenean ultricies mi vitae est. Mauris placerat
                                                                        eleifend
                                                                        leo.
                                                                    </p>
                                                                    <ul>
                                                                        <li>
                                                                            Lorem ipsum dolor sit amet, consectetuer
                                                                        </li>
                                                                        <li>
                                                                            Aliquam tincidunt mauris eu risus
                                                                        </li>
                                                                        <li>
                                                                            Vestibulum auctor dapibus neque
                                                                        </li>
                                                                        <li>
                                                                            Morbi in sem quis dui placerat ornare
                                                                        </li>
                                                                        <li>
                                                                            Donec eu libero sit amet quam egestas semper
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab_c">
                                                    <div class="uni-our-service-2-content-default">
                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                <div class="item-img">
                                                                    <img src="images/services/img2.jpg" alt=""
                                                                        class="img-responsive">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-7">
                                                                <div class="item-caption">
                                                                    <div class="item-caption-title">
                                                                        <h3>General health check</h3>
                                                                        <div class="uni-line"></div>
                                                                    </div>
                                                                    <p>Pellentesque habitant morbi tristique senectus et
                                                                        netus
                                                                        et malesuada fames ac turpis egestas. Vestibulum
                                                                        tortor
                                                                        quam, feugiat vitae, ultricies eget, tempor sit
                                                                        amet,
                                                                        ante. Donec eu libero sit amet quam egestas
                                                                        semper.
                                                                        Aenean ultricies mi vitae est. Mauris placerat
                                                                        eleifend
                                                                        leo.
                                                                    </p>
                                                                    <ul>
                                                                        <li>
                                                                            Lorem ipsum dolor sit amet, consectetuer
                                                                        </li>
                                                                        <li>
                                                                            Aliquam tincidunt mauris eu risus
                                                                        </li>
                                                                        <li>
                                                                            Vestibulum auctor dapibus neque
                                                                        </li>
                                                                        <li>
                                                                            Morbi in sem quis dui placerat ornare
                                                                        </li>
                                                                        <li>
                                                                            Donec eu libero sit amet quam egestas semper
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab_d">
                                                    <div class="uni-our-service-2-content-default">
                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                <div class="item-img">
                                                                    <img src="images/services/img3.jpg" alt=""
                                                                        class="img-responsive">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-7">
                                                                <div class="item-caption">
                                                                    <div class="item-caption-title">
                                                                        <h3>Diagnosis & treatment cancer</h3>
                                                                        <div class="uni-line"></div>
                                                                    </div>
                                                                    <p>Pellentesque habitant morbi tristique senectus et
                                                                        netus
                                                                        et malesuada fames ac turpis egestas. Vestibulum
                                                                        tortor
                                                                        quam, feugiat vitae, ultricies eget, tempor sit
                                                                        amet,
                                                                        ante. Donec eu libero sit amet quam egestas
                                                                        semper.
                                                                        Aenean ultricies mi vitae est. Mauris placerat
                                                                        eleifend
                                                                        leo.
                                                                    </p>
                                                                    <ul>
                                                                        <li>
                                                                            Lorem ipsum dolor sit amet, consectetuer
                                                                        </li>
                                                                        <li>
                                                                            Aliquam tincidunt mauris eu risus
                                                                        </li>
                                                                        <li>
                                                                            Vestibulum auctor dapibus neque
                                                                        </li>
                                                                        <li>
                                                                            Morbi in sem quis dui placerat ornare
                                                                        </li>
                                                                        <li>
                                                                            Donec eu libero sit amet quam egestas semper
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab_e">
                                                    <div class="uni-our-service-2-content-default">
                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                <div class="item-img">
                                                                    <img src="images/services/img3.jpg" alt=""
                                                                        class="img-responsive">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-7">
                                                                <div class="item-caption">
                                                                    <div class="item-caption-title">
                                                                        <h3>Treatment of dermatitis</h3>
                                                                        <div class="uni-line"></div>
                                                                    </div>
                                                                    <p>Pellentesque habitant morbi tristique senectus et
                                                                        netus
                                                                        et malesuada fames ac turpis egestas. Vestibulum
                                                                        tortor
                                                                        quam, feugiat vitae, ultricies eget, tempor sit
                                                                        amet,
                                                                        ante. Donec eu libero sit amet quam egestas
                                                                        semper.
                                                                        Aenean ultricies mi vitae est. Mauris placerat
                                                                        eleifend
                                                                        leo.
                                                                    </p>
                                                                    <ul>
                                                                        <li>
                                                                            Lorem ipsum dolor sit amet, consectetuer
                                                                        </li>
                                                                        <li>
                                                                            Aliquam tincidunt mauris eu risus
                                                                        </li>
                                                                        <li>
                                                                            Vestibulum auctor dapibus neque
                                                                        </li>
                                                                        <li>
                                                                            Morbi in sem quis dui placerat ornare
                                                                        </li>
                                                                        <li>
                                                                            Donec eu libero sit amet quam egestas semper
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>

                </section>
            </div>

            <footer class="site-footer footer-default">
                <div class="copyright-area">
                    <div class="container">
                        <div class="copyright-content">
                            <div class="row">
                                <div class="col-sm-6">
                                </div>
                                <div class="col-sm-6">
                                    <ul class="copyright-menu">
                                        <li><a href="#"></a></li>
                                        <li><a href="#"></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <script src="plugin/jquery/jquery-2.0.2.min.js"></script>
    <script src="plugin/jquery-ui/jquery-ui.min.js"></script>
    <script src="plugin/bootstrap/js/bootstrap.js"></script>
    <script src="plugin/process-bar/tox-progress.js"></script>
    <script src="plugin/waypoint/jquery.waypoints.min.js"></script>
    <script src="plugin/counterup/jquery.counterup.min.js"></script>
    <script src="plugin/owl-carouse/owl.carousel.min.js"></script>
    <script src="plugin/jquery-ui/jquery-ui.min.js"></script>
    <script src="plugin/mediaelement/mediaelement-and-player.js"></script>
    <script src="plugin/masonry/masonry.pkgd.min.js"></script>
    <script src="plugin/datetimepicker/moment.min.js"></script>
    <script src="plugin/datetimepicker/bootstrap-datepicker.min.js"></script>
    <script src="plugin/datetimepicker/bootstrap-datepicker.tr.min.js"></script>
    <script src="plugin/datetimepicker/bootstrap-datetimepicker.js"></script>
    <script src="plugin/datetimepicker/bootstrap-datetimepicker.fr.js"></script>

    <script src="plugin/lightgallery/picturefill.min.js"></script>
    <script src="plugin/lightgallery/lightgallery.js"></script>
    <script src="plugin/lightgallery/lg-pager.js"></script>
    <script src="plugin/lightgallery/lg-autoplay.js"></script>
    <script src="plugin/lightgallery/lg-fullscreen.js"></script>
    <script src="plugin/lightgallery/lg-zoom.js"></script>
    <script src="plugin/lightgallery/lg-hash.js"></script>
    <script src="plugin/lightgallery/lg-share.js"></script>
    <script src="plugin/sticky/jquery.sticky.js"></script>

    <script src="js/main.js"></script>
</body>

<!-- 02_01_about.php  [XR&CO'2014], Tue, 22 Oct 2019 11:54:55 GMT -->

</html>