-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 03, 2019 at 10:14 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `labinfo_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_doctor`
--

CREATE TABLE `tbl_doctor` (
  `doctor_id` int(11) NOT NULL,
  `doctor_image` varchar(255) NOT NULL,
  `doctor_fname` varchar(255) NOT NULL,
  `doctor_lname` varchar(255) NOT NULL,
  `doctor_qualification` varchar(255) NOT NULL,
  `doctor_classification` varchar(255) NOT NULL,
  `doctor_email` varchar(255) NOT NULL,
  `doctor_phone` varchar(255) NOT NULL,
  `doctor_dateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_doctor`
--

INSERT INTO `tbl_doctor` (`doctor_id`, `doctor_image`, `doctor_fname`, `doctor_lname`, `doctor_qualification`, `doctor_classification`, `doctor_email`, `doctor_phone`, `doctor_dateCreated`) VALUES
(2, '7810-51bcBpTG28L._SX425_.jpg', 'Davie Chris', 'Fano', 'BSed', 'Surgeon', 'dv@gmail.com', '0997864523', '2019-12-03 23:36:57'),
(4, '8006-51bcBpTG28L._SX425_.jpg', 'EJ Anton', 'Potot', 'BSIR', 'progmmar', 'asdasd1', 'asdads', '2019-12-04 04:35:56');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_doctorpatient`
--

CREATE TABLE `tbl_doctorpatient` (
  `dp_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `dp_dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_doctorpatient`
--

INSERT INTO `tbl_doctorpatient` (`dp_id`, `doctor_id`, `patient_id`, `dp_dateCreated`) VALUES
(10, 2, 14, '2019-12-03 19:25:44'),
(11, 2, 15, '2019-12-03 19:26:48'),
(12, 2, 16, '2019-12-03 20:06:30'),
(13, 4, 17, '2019-12-03 21:05:05');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_patient`
--

CREATE TABLE `tbl_patient` (
  `patient_id` int(11) NOT NULL,
  `patient_fname` varchar(255) NOT NULL,
  `patient_lname` varchar(255) NOT NULL,
  `patient_age` varchar(255) NOT NULL,
  `patient_sex` varchar(255) NOT NULL,
  `patient_addr` varchar(255) NOT NULL,
  `patient_email` varchar(255) NOT NULL,
  `patient_phone` varchar(255) NOT NULL,
  `patient_dateCreated` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_patient`
--

INSERT INTO `tbl_patient` (`patient_id`, `patient_fname`, `patient_lname`, `patient_age`, `patient_sex`, `patient_addr`, `patient_email`, `patient_phone`, `patient_dateCreated`) VALUES
(14, 'Bravo', 'Alpgha1', '20 years', 'male', 'Test addres', 'j2Wgmail.com', 'sdfgdf', '2019-12-04 03:25:43'),
(15, 'Echo', 'Charlie', '27 years', 'female', 'Test addresaf', 'asdsdasd', 'sdfgdfasdas', '2019-12-04 03:26:48'),
(16, 'Foxtrot', 'MIlo', '12 years', 'male', 'asdasd', 'asdsad', 'asdasd', '2019-12-04 04:06:30'),
(17, 'John', 'hdfghj', '20 years', 'female', 'aasd', 'sgdhgf', 'tqeas1', '2019-12-04 05:05:04');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_test`
--

CREATE TABLE `tbl_test` (
  `test_id` int(11) NOT NULL,
  `test_name` varchar(255) NOT NULL,
  `test_price` decimal(10,2) NOT NULL,
  `test_dateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_test`
--

INSERT INTO `tbl_test` (`test_id`, `test_name`, `test_price`, `test_dateCreated`) VALUES
(16, 'Stool Testing', '800.00', '2019-12-04 00:17:44'),
(17, 'Uring testing', '800.00', '2019-12-04 00:17:51'),
(18, 'Whatevertesting', '100.00', '2019-12-04 00:18:04'),
(19, 'Last whatever', '513.20', '2019-12-04 00:18:15'),
(20, 'WAIT', '151.00', '2019-12-04 00:18:29');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_testofpatient`
--

CREATE TABLE `tbl_testofpatient` (
  `top_id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `test_value` varchar(255) NOT NULL,
  `test_dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_testofpatient`
--

INSERT INTO `tbl_testofpatient` (`top_id`, `test_id`, `patient_id`, `test_value`, `test_dateCreated`) VALUES
(11, 16, 14, '12', '2019-12-03 19:25:44'),
(12, 18, 14, '2', '2019-12-03 19:25:44'),
(13, 20, 14, '12', '2019-12-03 19:25:44'),
(14, 16, 15, '', '2019-12-03 19:26:48'),
(15, 18, 15, '', '2019-12-03 19:26:48'),
(16, 19, 15, '', '2019-12-03 19:26:48'),
(17, 20, 15, '', '2019-12-03 19:26:48'),
(18, 16, 16, '3 ml', '2019-12-03 20:06:30'),
(19, 17, 16, '10mm', '2019-12-03 20:06:30'),
(20, 18, 16, '5 carabos', '2019-12-03 20:06:30'),
(22, 20, 16, '2.64 Gb', '2019-12-03 20:06:30'),
(23, 16, 17, '10mm', '2019-12-03 21:05:05'),
(24, 17, 17, '10mm', '2019-12-03 21:05:05'),
(25, 18, 17, '12', '2019-12-03 21:05:05'),
(26, 19, 17, '5', '2019-12-03 21:05:05'),
(27, 20, 17, '2', '2019-12-03 21:05:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_doctor`
--
ALTER TABLE `tbl_doctor`
  ADD PRIMARY KEY (`doctor_id`);

--
-- Indexes for table `tbl_doctorpatient`
--
ALTER TABLE `tbl_doctorpatient`
  ADD PRIMARY KEY (`dp_id`),
  ADD KEY `doctor_id` (`doctor_id`),
  ADD KEY `patient_id` (`patient_id`);

--
-- Indexes for table `tbl_patient`
--
ALTER TABLE `tbl_patient`
  ADD PRIMARY KEY (`patient_id`);

--
-- Indexes for table `tbl_test`
--
ALTER TABLE `tbl_test`
  ADD PRIMARY KEY (`test_id`);

--
-- Indexes for table `tbl_testofpatient`
--
ALTER TABLE `tbl_testofpatient`
  ADD PRIMARY KEY (`top_id`),
  ADD KEY `patient_id` (`patient_id`),
  ADD KEY `test_id` (`test_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_doctor`
--
ALTER TABLE `tbl_doctor`
  MODIFY `doctor_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_doctorpatient`
--
ALTER TABLE `tbl_doctorpatient`
  MODIFY `dp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tbl_patient`
--
ALTER TABLE `tbl_patient`
  MODIFY `patient_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tbl_test`
--
ALTER TABLE `tbl_test`
  MODIFY `test_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbl_testofpatient`
--
ALTER TABLE `tbl_testofpatient`
  MODIFY `top_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_doctorpatient`
--
ALTER TABLE `tbl_doctorpatient`
  ADD CONSTRAINT `tbl_doctorpatient_ibfk_1` FOREIGN KEY (`doctor_id`) REFERENCES `tbl_doctor` (`doctor_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_doctorpatient_ibfk_2` FOREIGN KEY (`patient_id`) REFERENCES `tbl_patient` (`patient_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_testofpatient`
--
ALTER TABLE `tbl_testofpatient`
  ADD CONSTRAINT `tbl_testofpatient_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `tbl_patient` (`patient_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_testofpatient_ibfk_2` FOREIGN KEY (`test_id`) REFERENCES `tbl_test` (`test_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
