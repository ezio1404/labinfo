<?php
session_start();
include 'dbhelper.php';
if(!$_SESSION){
    header("location:index.php");
}

$doctorList = getAllDoctor();

?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Medicare +</title>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.png" />
    <link rel="stylesheet" href="plugin/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="plugin/bootstrap/css/bootstrap-theme.css">
    <link rel="stylesheet" href="fonts/poppins/poppins.css">
    <link rel="stylesheet" href="plugin/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="plugin/jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" href="plugin/process-bar/tox-progress.css">
    <link rel="stylesheet" href="plugin/owl-carouse/owl.carousel.min.css">
    <link rel="stylesheet" href="plugin/owl-carouse/owl.theme.default.min.css">
    <link rel="stylesheet" href="plugin/animsition/css/animate.css">
    <link rel="stylesheet" href="plugin/jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" href="plugin/mediaelement/mediaelementplayer.css">
    <link rel="stylesheet" href="plugin/datetimepicker/bootstrap-datepicker3.css">
    <link rel="stylesheet" href="plugin/datetimepicker/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="plugin/lightgallery/lightgallery.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="css/buttons.dataTables.min.css">

</head>

<body>

    <!--load page-->
    <div class="load-page">
        <div class="sk-wave">
            <div class="sk-rect sk-rect1"></div>
            <div class="sk-rect sk-rect2"></div>
            <div class="sk-rect sk-rect3"></div>
            <div class="sk-rect sk-rect4"></div>
            <div class="sk-rect sk-rect5"></div>
        </div>
    </div>

    <!-- Mobile nav -->
    <nav class="visible-sm visible-xs mobile-menu-container mobile-nav">
        <div class="menu-mobile-nav navbar-toggle">
            <span class="icon-bar"><i class="fa fa-bars" aria-hidden="true"></i></span>
        </div>
        <div id="cssmenu" class="animated">
            <div class="uni-icons-close"><i class="fa fa-times" aria-hidden="true"></i></div>
            <ul class="nav navbar-nav animated">
                <!-- if not logged in -->
                <li><a href="index.php">Home</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="contact.php">Contact</a></li>
                <li><a href="gallery.php">Gallery</a></li>
                <li><a href="departments.php">Departments</a></li>
                <li><a href="doctors.php">Doctors</a></li>

                <!-- hide if logged in else then-->
                <li><a href="dashboard.php?message=">Dashboard</a></li>
                <li><a href="manageDoctor.php?message=">Manage Doctor</a></li>
                <li><a href="manageTest.php?message=">Manange Test</a></li>
                <li><a href="patientList.php?message=">Patient List</a></li>
                <li class="active"><a href="doctorList.php?message=">Doctor List</a></li>
                <li><a href="logout.php">Logout</a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
    </nav>
    <!-- End mobile menu -->

    <div class="uni-departments">
        <div id="wrapper-container" class="site-wrapper-container">
            <header>
                <div class="uni-medicare-header sticky-menu">
                    <div class="container">
                        <div class="uni-medicare-header-main">
                            <div class="row">
                                <div class="col-md-2">
                                    <!--LOGO-->
                                    <div class="wrapper-logo">
                                        <a class="logo-default" href="#"><img src="images/logo.png" alt=""
                                                class="img-responsive"></a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <!--MENU TEXT-->
                                    <div class="uni-main-menu">
                                        <nav class="main-navigation uni-menu-text">
                                            <div class="cssmenu">
                                                <ul>
                                                    <!-- if not logged in -->
                                                    <?php
                                                        if($_SESSION['user'] != "admin"){
                                                    ?>
                                                    <li><a href="index.php">Home</a></li>
                                                    <li><a href="about.php">About</a></li>
                                                    <li><a href="contact.php">Contact</a></li>
                                                    <li><a href="gallery.php">Gallery</a></li>
                                                    <li><a href="departments.php">Departments</a></li>
                                                    <li><a href="login.php">Login</a></li>
                                                    <li><a href="doctors.php">Doctors</a></li>
                                                    <?php
                                                        }else{
                                                    ?>
                                                    <!-- hide if logged in else then-->
                                                    <li><a href="dashboard.php?message=">Home</a></li>
                                                    <li><a href="manageDoctor.php?message="> Doctor</a></li>
                                                    <li><a href="manageTest.php?message="> Test</a></li>
                                                    <li><a href="patientList.php?message=">Patient List</a></li>
                                                    <li class="active"><a href="doctorList.php?message=">Doctor List</a>
                                                    </li>
                                                    <li><a href="logout.php">Logout</a></li>
                                                    <?php
                                                        }
                                                    ?>
                                                </ul>
                                            </div>
                                        </nav>
                                    </div>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>
            </header>

            <div id="main-content" class="site-main-content">
                <section class="site-content-area">



                    <div class="uni-departments-body" style="padding-top:100px !important;">
                        <div class="container">
                            <div class="row ">
                                <div class="col-md-12">
                                    <table class="table table-bordered table-hover" id="example" style="width:100%">
                                        <thead>
                                            <th>Name</th>
                                            <th>Degree</th>
                                            <th>Classification</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Action</th>
                                        </thead>
                                        <tbody>
                                            <?php
                                        foreach($doctorList as $doctorData){
                                        ?>
                                            <tr>
                                                <td><?php echo $doctorData['doctor_lname'].",".$doctorData['doctor_fname']?>
                                                </td>
                                                <td><?php echo $doctorData['doctor_qualification'] ?></td>
                                                <td><?php echo $doctorData['doctor_classification'] ?></td>
                                                <td><?php echo $doctorData['doctor_email'] ?></td>
                                                <td><?php echo $doctorData['doctor_phone'] ?></td>
                                                <td>
                                                    <a class="btn btn-outline-success"
                                                        href="viewDoctor.php?doctorid=<?php echo $doctorData['doctor_id']?>"><i
                                                            class="fa fa-user-md"></i></a>
                                                    <a class="btn btn-outline-primary"
                                                        href="updateDoctor.php?id=<?php echo $doctorData['doctor_id']?>"><i
                                                            class="fa fa-edit"></i></a>
                                                    <a class="btn  btn-outline-danger"
                                                        href="deleteDoctor.php?id=<?php echo $doctorData['doctor_id']?>&data=<?php echo  $doctorData['doctor_lname'].",".$doctorData['doctor_fname']?>"><i
                                                            class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                            ?>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>


                </section>
            </div>
            <br><br><br><br><br><br><br>

            <br><br><br><br><br><br><br> <br><br><br><br><br><br><br>
            <footer class="site-footer footer-default">
                <div class="copyright-area">
                    <div class="container">
                        <div class="copyright-content">
                            <div class="row">
                                <div class="col-sm-6">
                                    <p class="copyright-text"> </p>
                                </div>
                                <div class="col-sm-6">
                                    <ul class="copyright-menu">
                                        <li><a href="#"></a></li>
                                        <li><a href="#"></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <script src="plugin/jquery/jquery-2.0.2.min.js"></script>
    <script src="plugin/jquery-ui/jquery-ui.min.js"></script>
    <script src="plugin/bootstrap/js/bootstrap.js"></script>
    <script src="plugin/process-bar/tox-progress.js"></script>
    <script src="plugin/waypoint/jquery.waypoints.min.js"></script>
    <script src="plugin/counterup/jquery.counterup.min.js"></script>
    <script src="plugin/owl-carouse/owl.carousel.min.js"></script>
    <script src="plugin/jquery-ui/jquery-ui.min.js"></script>
    <script src="plugin/mediaelement/mediaelement-and-player.js"></script>
    <script src="plugin/masonry/masonry.pkgd.min.js"></script>
    <script src="plugin/datetimepicker/moment.min.js"></script>
    <script src="plugin/datetimepicker/bootstrap-datepicker.min.js"></script>
    <script src="plugin/datetimepicker/bootstrap-datepicker.tr.min.js"></script>
    <script src="plugin/datetimepicker/bootstrap-datetimepicker.js"></script>
    <script src="plugin/datetimepicker/bootstrap-datetimepicker.fr.js"></script>

    <script src="plugin/lightgallery/picturefill.min.js"></script>
    <script src="plugin/lightgallery/lightgallery.js"></script>
    <script src="plugin/lightgallery/lg-pager.js"></script>
    <script src="plugin/lightgallery/lg-autoplay.js"></script>
    <script src="plugin/lightgallery/lg-fullscreen.js"></script>
    <script src="plugin/lightgallery/lg-zoom.js"></script>
    <script src="plugin/lightgallery/lg-hash.js"></script>
    <script src="plugin/lightgallery/lg-share.js"></script>
    <script src="plugin/sticky/jquery.sticky.js"></script>

    <script src="js/main.js"></script>

    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.buttons.min.js"></script>
    <script src="js/buttons.print.min.js"></script>
    <script src="js/buttons.flash.min.js"></script>
    <script src="js/buttons.html5.min.js"></script>
    <script src="js/jszip.min.js"></script>
    <script src="js/pdfmake.min.js"></script>
    <script src="js/vfs_fonts.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').DataTable({
                "pageLength": 20,
                dom: 'Bfrtip',
                buttons: ['copy', 'csv', 'excel', 'pdf', 'print']
            });
        });
    </script>
</body>

</html>